import React from "react";
import Salad from './lab1'
import inventory from "./inventory.ES6";



class ComposeSalad extends React.Component {
constructor(props){
  super(props);
  //this.handleSubmit = this.handleSubmit.bind(this);
  this.state = {
    salad: new Salad()
  }; 
  //this.state.salad.add("foundation", "Sallad");
  //this.state.salad.add("dressing", "Ceasardressing"); //ej dynamiskt, kan behöva ändras.
}

createEmptySalad(){
  let newSalad = new Salad();
  //newSalad.add("foundation", "Sallad");
  //newSalad.add("dressing", "Ceasardressing"); //ej dynamiskt, kan behöva ändras.
  this.setState( {
    salad: newSalad
  }); 
}

handleSubmit = (e) => {
  e.preventDefault();
  //this.props.state.salads.push(this.state.salad);
  if(e.target.checkValidity() === false){ 
    e.target.classList.add("was-validated");
   } else {
  this.props.obj.onSubmit(this.state.salad);
  document.getElementById("saladForm").reset(); //behöver ej den här med bundna komponenter
  this.createEmptySalad();
  this.props.history.push("/view-order"); 
  e.target.classList.remove("was-validated"); //måste ta bort valideringen
}
}

selectChange = (e) => {
  if(inventory[e.target.value]){
 if(inventory[e.target.value].dressing){
this.state.salad.add("dressing", e.target.value);
 } else if(inventory[e.target.value].foundation){
  this.state.salad.add("foundation", e.target.value);
 }
}
 console.log(this.state.salad);
}

checkboxChange = (e) => {
if(e.target.checked){
  if(inventory[e.target.value].extra){
    this.state.salad.add("extra", e.target.value);
     } else if(inventory[e.target.value].protein){
      this.state.salad.add("protein", e.target.value);
     }
} else {
  if(inventory[e.target.value].extra){
    this.state.salad.remove("extra", e.target.value);
     } else if(inventory[e.target.value].protein){
      this.state.salad.remove("protein", e.target.value);
     }
}
  console.log(this.state.salad);
 }

  render() {
    const inventory = this.props.inventory;
    // test for correct ussage, the parent must send this datastructure
    if (!inventory) {
      alert("inventory is undefined in ComposeSalad");
    }
    let foundations = Object.keys(inventory).filter(
      name => inventory[name].foundation
    );
    let extras = Object.keys(inventory).filter(
        name => inventory[name].extra
      );

    let proteins = Object.keys(inventory).filter(
        name => inventory[name].protein
      );
    let dressings = Object.keys(inventory).filter(
        name => inventory[name].dressing
      );
//value=this.state bundna komponenter, för checkbox också, mappa true /false
    return (
      <div>
      <div className="jumbotron text-center">
      <h1>My Own Salad Bar</h1>
      <p>Here you can order custom made salads!</p> 
      </div>
      <div className="container">
                  <form id="saladForm" onSubmit={this.handleSubmit} noValidate >   
        <h4>Välj bas</h4>

        <div className="form-group">
<label htmlFor="foundationSelect">Select foundation</label>
        <select required value="Sallad" className="form-control" id="foundationSelect" onChange = { e => this.selectChange(e)}> 
            <option value="">make a choice...</option>
          {foundations.map(name=> (
            <option value={name} id={name}>{name}+{inventory[name].price} kr</option>
          ))}
</select>
<div className="invalid-feedback">required, select one</div>
</div>

        <h4>Välj protein</h4>

          <ul>
          {proteins.map(name => (
            <li key={name}>
            <input value={name} onChange = { e => this.checkboxChange(e)} type="checkbox" id={name}/>{name} +{inventory[name].price } kr
            </li>
          ))}
          </ul>
          <h4>Välj extras</h4>
        <ul>
          {extras.map(name => (
            <li key={name}>
            <input value={name} onChange = { e => this.checkboxChange(e)} type="checkbox" id={name}/>{name}+{inventory[name].price} kr
            </li>
          ))}
          </ul>
          <h4>Välj dressing</h4>

          <div className="form-group">
          <label htmlFor="dressingSelect">Select dressing</label>
          <select required className="form-control" id="dressingSelect" onChange = { e => this.selectChange(e)}>
            <option value="">make a choice...</option>
            {dressings.map(name=> (
            <option value={name} id={name}>{name} +{inventory[name].price} kr</option> 
          ))}
          </select> 
          <div className="invalid-feedback">required, select one</div>
          </div>
        
        <input type="submit" value="Submit"/>
        </form>
      </div>
      </div>
    );
  }
}

export default ComposeSalad;