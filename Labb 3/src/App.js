import React from "react";
import ReactDOM from 'react-dom';
//import { Route, Link, Switch } from "react-dom";
//import React from 'react';;
import logo from './logo.svg';
import './App.css';
import Salad from './lab1'
import inventory from './inventory.ES6';
import ComposeSalad from './ComposeSalad';
import ViewOrder from './ViewOrder';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
        // in your render() function, add to existing JSX:
        

        class App extends React.Component {
          constructor(props){
            super(props);
            this.state = {
              salads: [],
              //inventory: {}
          };
        }

        composeSaladElem = params => (
        <ComposeSalad {...params} obj={this} inventory={inventory} /> //this.onSubmit
        );

        viewOrderElem = params => (
              <ViewOrder {...params} order={this.state.salads} />
        );



        onSubmit = (e) => {
          let temp = this.state.salads;
          temp.push(e);
          this.setState(
            { salads: temp
            }
            );
          console.log("state", this.state);
        

        }

/*
          addSalad = obj => {
         //   obj.id = shortid.generate();
         //   obj.price = this.calculatePrice(obj);
    
            const salad = [...this.state.salads, obj];
            localStorage.setItem("salads", JSON.stringify(salad));
            this.setState({
                salads: salad
            });
        }; */
        



    render() {

        return (
            
      <div>
        <Router>
            <ul className="nav nav-pills">
<li className="nav-item">
<Link className="nav-link" to="compose-salad">Beställ sallad</Link>
</li>
<li className="nav-item">
<Link className="nav-link" to="view-order">Din beställning</Link>
</li>
</ul>
                        <Route path="/compose-salad" render={this.composeSaladElem}/>
                        <Route path="/view-order" render={this.viewOrderElem} />
                    </Router>






    </div>
    );
        
  

        }
      }

//ReactDOM.render( < App / > , document.getElementById('root'));

export default App;

