import React, { Component } from "react";

class ViewOrder extends Component {
    render() {
            const order = this.props.order;
            return (
            //test
<div>
            <div className="jumbotron text-center">
            <h1>My Own Salad Bar</h1>
            <p>Here you can order custom made salads!</p> 
           </div>

            <div className="container">
  <h4>Din beställning</h4>
    {order.map(name=> (
    <li key={name.getId()}>{name.foundation}, {name.protein}, {name.extra}, {name.dressing}, price: {name.price()} kr</li>
    ))}
      </div>
            </div>
            );
        }
    }
    
    export default ViewOrder;