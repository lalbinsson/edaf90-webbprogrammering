import React from "react";
import ReactDOM from 'react-dom';
//import { Route, Link, Switch } from "react-dom";
//import React from 'react';;
import logo from './logo.svg';
import './App.css';
import Salad from './lab1'
//import inventory from './inventory.ES6';
import ComposeSalad from './ComposeSalad';
import ViewOrder from './ViewOrder';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
        // in your render() function, add to existing JSX:
        

        class App extends React.Component {
          constructor(props){
            super(props);
           this.onSubmit = this.onSubmit.bind(this);
            this.state = {
              salads: [],
              inventory: {}
          };
        }
        

        composeSaladElem = params => (
        <ComposeSalad {...params} obj={this.onSubmit} inventory={this.state.inventory} /> //this.onSubmit
    
        );

        viewOrderElem = params => (
              <ViewOrder {...params} place={this.placeOrder} clear={this.clearOrder} order={this.state.salads}/>
        );

        clearOrder = () => {
            console.log("clearing order");
            this.setState({ salads: [] });
            window.localStorage.setItem("salads", JSON.stringify([]));
        }
    
        placeOrder = () => {
            console.log("placing order");
            console.log("order:" +JSON.stringify(this.state.salads));
            fetch("http://localhost:8080/orders/", {
                method: "POST",
                headers: new Headers(),
                mode: "cors",
                cache: "default",
                body: JSON.stringify(this.state.salads)
            })

                .then(res => {

                    if(!res.ok){
                        throw new Error("Network response was not ok");
                    }
                    return res.json();
                })
                .then(res => {
                    console.log(res); //fråga angående terminal-kommando.
                })
                .catch((error) => {
                    console.error("There has been a problem with your fetch operation:", error);
                });
                this.clearOrder();
            };
        
        onSubmit = (e) => {
          let temp = this.state.salads;
          temp.push(e);
          this.setState(
            { salads: temp
            }
            );
            localStorage.setItem("salads", JSON.stringify(temp));
        }

        componentDidMount() {
            console.log(this.state.salads);
            let tempArray = JSON.parse(localStorage.getItem("salads"));
            console.log(tempArray);
            let newArray = [];
            var i= 0;
            for( i; i<tempArray.length; i++){
    let s = Object.setPrototypeOf(tempArray[i], Salad.prototype);
    newArray.push(s);
            }
            this.setState(
                { salads: newArray
                }
                );
                console.log(newArray);
                console.log(this.state.salads);

          let inventory = {};
          const urls = ["foundations", "proteins", "extras", "dressings"];
          Promise.all(
              urls.map(data => {
                  const url = new URL(data, "http://localhost:8080/");
                  return fetch(url, {
                      method: "GET",
                      headers: new Headers(),
                      mode: "cors",
                      cache: "default"
                  })
                      .then(res => res.json())
                      .then(res => {
                          Promise.all(
                              res.map(key => {
                                  const url2 = new URL(key, url.toString() + "/");
                                  return fetch(url2, {
                                      method: "GET",
                                      headers: new Headers(),
                                      mode: "cors",
                                      cache: "default"
                                  })
                                      .then(res => res.json())
                                      .then(res => {
                                          inventory[key] = res;
                                      });
                              })
                          ).then(() => {
                              this.setState({ inventory: inventory });
                          });
                      });
              })
          );
          console.log(this.state.inventory);
console.log(this.state.salads);
            }

    render() {


console.log(this.state.salads);
      
      let urlfoundations = 'http://localhost:8080/foundations';
      fetch(urlfoundations)
      .then(response => response.json())
      .then(result => this.state.inventory.foundations = result)
     // .then(data => this.state.inventory.foundations.push(data));
      console.log(this.state.inventory.foundations);
      let urlproteins = 'http://localhost:8080/proteins';
      fetch(urlproteins)
      .then(response => response.json())
      .then(result => this.state.inventory.proteins = result)
      //.then(data => this.state.inventory.proteins.push(data));
      console.log(this.state.inventory.proteins);
      let urlextras = 'http://localhost:8080/extras';
      fetch(urlextras)
      .then(response => response.json())
      .then(result => this.state.inventory.extras = result)
      //.then(data => this.state.inventory.extras.push(data));
      console.log(this.state.inventory.extras);
      let urldressings = 'http://localhost:8080/dressings';
      fetch(urldressings)
      .then(response => response.json())
      .then(result => this.state.inventory.dressings = result)
      //.then(data => this.state.inventory.dressings.push(data));
      console.log(this.state.inventory.dressings);
      console.log(this.state.salad);


        return (

      <div>
        <Router>
            <ul className="nav nav-pills">
<li className="nav-item">
<Link className="nav-link" to="compose-salad">Beställ sallad</Link>
</li>
<li className="nav-item">
<Link className="nav-link" to="view-order">Din beställning</Link>
</li>
</ul>
                        <Route path="/compose-salad" render={this.composeSaladElem}/>
                        <Route path="/view-order" render={this.viewOrderElem} />
                    </Router>






    </div>
    );

        }
      }

//ReactDOM.render( < App / > , document.getElementById('root'));


export default App;

