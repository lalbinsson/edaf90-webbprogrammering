import React, { Component } from "react";

class ViewOrder extends Component {
  constructor(props){
    super(props);
  }


    render() {
            const order = this.props.order;
            return (
            //test
<div>
            <div className="jumbotron text-center">
            <h1>My Own Salad Bar</h1>
           </div>

            <div className="container">
  <h4>Din beställning</h4>
    {order.map(name=> (
    <li key={name.getId()}>{name.foundation}, {name.protein}, {name.extra}, {name.dressing}, price: {name.price()} kr</li>
    ))}
      </div>
      <button onClick={this.props.place}>Place order</button>
      <button onClick={this.props.clear}>Clear order</button>
            </div>
            );
        }
    }
    
    export default ViewOrder;