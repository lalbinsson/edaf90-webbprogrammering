// ’use strict’;
const imported = require("./inventory.js");

//uppg 4
var result = { //skapar ett  objekt med olika resultat.
	foundation: "Foundations: ",
	protein: "Proteins: ",
	extra: "Extras: ",
	dressing: "Dressings: "
};

const items = Object.keys(imported.inventory); //alla objekt i 
items.forEach(key => {
	if ("foundation" in imported.inventory[key]) { //om foundation finns med i item(key), lägg till keyn i rätt results.
		result.foundation += key + ",";
	} else if ("protein" in imported.inventory[key]) {
		result.protein += key + ",";
	} else if ("extra" in imported.inventory[key]) {
		result.extra += key + ",";
	} else if ("dressing" in imported.inventory[key]) {
		result.dressing += key + ",";
	}
});

console.log(
	result.foundation +
	"\n" +
	result.protein +
	"\n" +
	result.extra +
	"\n" +
	result.dressing
);

//uppg 5
class Salad {
	constructor() {
		this.foundation = [];
		this.protein = [];
		this.extra = [];
		this.dressing = [];
		this.id = Date.now();
		//this.foundation.push("");
		//this.dressing.push("");
	}

	add(type, selection) {
		if (type === "foundation") {
			this.foundation.push(selection);
		} else if (type === "protein") {
			this.protein.push(selection);
		} else if (type === "extra") {
			this.extra.push(selection);
		} else if (type === "dressing") {
			this.dressing.push(selection);
		} else {
			console.error("type not available");
		}

	}

	remove(type, selection) {
		if (type === "foundation") {
			this.foundation.splice(selection);
		} else if (type === "protein") {
			this.protein.splice(selection);
		} else if (type === "extra") {
			this.extra.splice(selection);
		} else if (type === "dressing") {
			this.dressing.splice(selection);
		} else {
			console.error("type not available");
		}

	}

	getId(){
		return this.id;
	}


	//uppg 7
	price() {
		return this.foundation
			.concat(this.protein, this.extra, this.dressing)
			.reduce((acc, curr) => {
				return (acc += imported.inventory[curr].price); //fattar ej reduce
			}, 0);
	}
}

//uppg 6
let myCeasarsalad = new Salad();
myCeasarsalad.add("foundation", "Sallad + Glasnudlar");
myCeasarsalad.add("protein", "Norsk fjordlax");
myCeasarsalad.add("extra", "Avocado");
myCeasarsalad.add("extra", "Tomat");
myCeasarsalad.add("dressing", "Kimchimayo");
console.log(myCeasarsalad);
console.log(myCeasarsalad.price());

//uppg 8
class ExtraGreensalad extends Salad {
	price() {
		return this.foundation
			.concat(this.protein, this.extra, this.dressing)
			.reduce((acc, curr) => {
				if ("foundation" in imported.inventory[curr]) {
					return (acc += imported.inventory[curr].price * 1.3);
				} else {
					return (acc += imported.inventory[curr].price * 0.5);
				}
			}, 0);
	}
}

let myExtrasalad = new ExtraGreensalad();
myExtrasalad.add("foundation", "Sallad + Glasnudlar");
myExtrasalad.add("protein", "Norsk fjordlax");
myExtrasalad.add("extra", "Avocado");
myExtrasalad.add("extra", "Tomat");
myExtrasalad.add("dressing", "Kimchimayo");
console.log(myExtrasalad);
console.log(myExtrasalad.price());

//uppg: describe property chain


//uppg 10

class GourmetSalad extends Salad {
	add(type, selection, size = 1) {
		if (type === "foundation") {
			this.foundation.push({
				selection: selection,
				size: size
			});
		} else if (type === "protein") {
			this.protein.push({
				selection: selection,
				size: size
			});
		} else if (type === "extra") {
			this.extra.push({
				selection: selection,
				size: size
			});
		} else if (type === "dressing") {
			this.dressing.push({
				selection: selection,
				size: size
			});
		} else {
			console.error("type not available");
		}

	}

	price() {
		return this.foundation
			.concat(this.protein, this.extra, this.dressing)
			.reduce((acc, curr) => {
				return (
					acc + imported.inventory[curr.selection].price * curr.size
				);
			}, 0);
	}
}

let myGourmetsalad = new GourmetSalad();

myGourmetsalad.add("foundation", "Sallad + Glasnudlar");
myGourmetsalad.add("protein", "Norsk fjordlax", 0.2);
myGourmetsalad.add("extra", "Tomat", 0.2);
myGourmetsalad.add("extra", "Parmesan", 0.2);
myGourmetsalad.add("dressing", "Kimchimayo", 0.2);
myGourmetsalad.remove("extra", "Parmesan", 0.2);
console.log(myGourmetsalad);
console.log(myGourmetsalad.price());

export default Salad;