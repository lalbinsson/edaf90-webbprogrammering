import React from "react";
import Salad from './lab1'
//import inventory from "./inventory.ES6";



class ComposeSalad extends React.Component {
constructor(props){
  super(props);
  //this.onSubmit = this.onSubmit.bind(this);
  this.state = {
    Salad: new Salad()
    
  }; 
}

createEmptySalad(){
  let newSalad = new Salad();
  this.setState( {
    Salad: newSalad
  }); 
}

handleSubmit = (e) => {
  e.preventDefault();
  if(e.target.checkValidity() === false){ 
    
   } else {
  this.props.obj(this.state.Salad);
  document.getElementById("saladForm").reset();
  this.createEmptySalad();
  e.target.classList.remove("was-validated");
  this.props.history.push("/view-order");
}
}

selectChange = (e) => {
  if(this.props.inventory[e.target.value]){
 if(this.props.inventory[e.target.value].dressing){
this.state.Salad.add("dressing", e.target.value);
 } else if(this.props.inventory[e.target.value].foundation){
  this.state.Salad.add("foundation", e.target.value);
 }
}
}

checkboxChange = (e) => {

if(e.target.checked){
  this.state.Salad.add("extra", e.target.value);  
  if(this.props.inventory[e.target.value].extra){
     } else if(this.props.inventory[e.target.value].protein){
      this.state.Salad.add("protein", e.target.value);
     }
} else {
  if(this.props.inventory[e.target.value].extra){
    this.state.Salad.remove("extra", e.target.value);
     } else if(this.props.inventory[e.target.value].protein){
      this.state.Salad.remove("protein", e.target.value);
     }
}
 } 

  render() {
    const inventory = this.props.inventory;
    if (!inventory) {
      alert("inventory is undefined in ComposeSalad");
    }
    let foundations = Object.keys(inventory).filter(
      name => inventory[name].foundation
    );
    let extras = Object.keys(inventory).filter(
        name => inventory[name].extra
      );

    let proteins = Object.keys(inventory).filter(
        name => inventory[name].protein
      );
    let dressings = Object.keys(inventory).filter(
        name => inventory[name].dressing
      );
      foundations.map(name=> (
        console.log(name)));

    return (
      <div>
      <div className="jumbotron text-center">
      <h1>My Own Salad Bar</h1>
      <p>Here you can order custom made salads!</p> 
      </div>
      <div className="container">
                  <form id="saladForm" onSubmit={this.handleSubmit} noValidate >   
        <h4>Välj bas</h4>

        <div className="form-group">
<label htmlFor="foundationSelect">Select foundation</label>
        <select  required className="form-control" id="foundationSelect" onChange = { e => this.selectChange(e)}>
            <option value="">make a choice...</option>
          {foundations.map(name=> (
            <option value={name} id={name} key={name}>{name}+{inventory[name].price} kr</option>
          ))}
</select>
<div className="invalid-feedback">required, select one</div>
</div>

        <h4>Välj protein</h4>

          <ul>
          {proteins.map(name => (
            <li key={name}>
            <input value={name} onChange = { e => this.checkboxChange(e)} type="checkbox" id={name}/>{name} +{inventory[name].price } kr
            </li>
          ))}
          </ul>
          <h4>Välj extras</h4>
        <ul>
          {extras.map(name => (
            <li key={name}>
            <input value={name} onChange = { e => this.checkboxChange(e)} type="checkbox" id={name}/>{name}+{inventory[name].price} kr
            </li>
          ))}
          </ul>
          <h4>Välj dressing</h4>

          <div className="form-group">
          <label htmlFor="dressingSelect">Select dressing</label>
          <select required  className="form-control" id="dressingSelect" onChange = { e => this.selectChange(e)}>
            <option value="">make a choice...</option>
            {dressings.map(name=> (
            <option value={name} id={name}>{name} +{inventory[name].price} kr</option> 
          ))}
          </select> 
          <div className="invalid-feedback">required, select one</div>
          </div>
        
        <input type="submit" value="Submit"/>
        </form>
      </div>
      </div>
    );
  }
}

export default ComposeSalad;