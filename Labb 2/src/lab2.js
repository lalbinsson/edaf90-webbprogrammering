// ’use strict’;
const imported = require("./inventory.js");

//uppg 4
var result = { //skapar ett  objekt med olika resultat.
	foundation: "Foundations: ",
	protein: "Proteins: ",
	extra: "Extras: ",
	dressing: "Dressings: "
};


const items = Object.keys(imported.inventory); //alla objekt i 
items.forEach(key => {
	if ("foundation" in imported.inventory[key]) { //om foundation finns med i item(key), lägg till keyn i rätt results.
		result.foundation += key + ",";
	} else if ("protein" in imported.inventory[key]) {
		result.protein += key + ",";
	} else if ("extra" in imported.inventory[key]) {
		result.extra += key + ",";
	} else if ("dressing" in imported.inventory[key]) {
		result.dressing += key + ",";
	}
});

console.log(
	result.foundation +
	"\n" +
	result.protein +
	"\n" +
	result.extra +
	"\n" +
	result.dressing
);

//uppg 5
class Salad {
	constructor() {
		this.foundation = [];
		this.protein = [];
		this.extra = [];
		this.dressing = [];
		this.id = Date.now();
	}

	print(){
		return 
	}

	add(type, selection) {
		if (type == "foundation") {
			this.foundation.push(selection);
		} else if (type == "protein") {
			this.protein.push(selection);
		} else if (type == "extra") {
			this.extra.push(selection);
		} else if (type == "dressing") {
			this.dressing.push(selection);
		} else {
			console.error("type not available");
		}

	}

	remove(type, selection) {
		if (type == "foundation") {
			this.foundation.splice(selection);
		} else if (type == "protein") {
			this.protein.splice(selection);
		} else if (type == "extra") {
			this.extra.splice(selection);
		} else if (type == "dressing") {
			this.dressing.splice(selection);
		} else {
			console.error("type not available");
		}

	}

	getId(){
		return this.id;
	}


	//uppg 7
	price() {
		return this.foundation
			.concat(this.protein, this.extra, this.dressing)
			.reduce((acc, curr) => {
				return (acc += imported.inventory[curr].price); //fattar ej reduce
			}, 0);
	}
}

//uppg 6
let myCeasarSalad = new Salad();
myCeasarSalad.add("foundation", "Salad + Glasnudlar");
myCeasarSalad.add("protein", "Norsk fjordlax");
myCeasarSalad.add("extra", "Avocado");
myCeasarSalad.add("extra", "Tomat");
myCeasarSalad.add("dressing", "Kimchimayo");
console.log(myCeasarSalad);
console.log(myCeasarSalad.price());

//uppg 8
class ExtraGreenSalad extends Salad {
	price() {
		return this.foundation
			.concat(this.protein, this.extra, this.dressing)
			.reduce((acc, curr) => {
				if ("foundation" in imported.inventory[curr]) {
					return (acc += imported.inventory[curr].price * 1.3);
				} else {
					return (acc += imported.inventory[curr].price * 0.5);
				}
			}, 0);
	}
}

let myExtraSalad = new ExtraGreenSalad();
myExtraSalad.add("foundation", "Salad + Glasnudlar");
myExtraSalad.add("protein", "Norsk fjordlax");
myExtraSalad.add("extra", "Avocado");
myExtraSalad.add("extra", "Tomat");
myExtraSalad.add("dressing", "Kimchimayo");
console.log(myExtraSalad);
console.log(myExtraSalad.price());

//uppg: describe property chain


//uppg 10

class GourmetSalad extends Salad {
	add(type, selection, size = 1) {
		if (type == "foundation") {
			this.foundation.push({
				selection: selection,
				size: size
			});
		} else if (type == "protein") {
			this.protein.push({
				selection: selection,
				size: size
			});
		} else if (type == "extra") {
			this.extra.push({
				selection: selection,
				size: size
			});
		} else if (type == "dressing") {
			this.dressing.push({
				selection: selection,
				size: size
			});
		} else {
			console.error("type not available");
		}

	}

	price() {
		return this.foundation
			.concat(this.protein, this.extra, this.dressing)
			.reduce((acc, curr) => {
				return (
					acc + imported.inventory[curr.selection].price * curr.size
				);
			}, 0);
	}
}

let myGourmetSalad = new GourmetSalad();

myGourmetSalad.add("foundation", "Salad + Glasnudlar");
myGourmetSalad.add("protein", "Norsk fjordlax", 0.2);
myGourmetSalad.add("extra", "Tomat", 0.2);
myGourmetSalad.add("extra", "Parmesan", 0.2);
myGourmetSalad.add("dressing", "Kimchimayo", 0.2);
myGourmetSalad.remove("extra", "Parmesan", 0.2);
console.log(myGourmetSalad);
console.log(myGourmetSalad.price());

export default Salad;